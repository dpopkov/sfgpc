package learn.sfg.sfgpc.services.map;

import learn.sfg.sfgpc.model.VetSpecialty;
import learn.sfg.sfgpc.services.VetSpecialtyService;
import org.springframework.stereotype.Service;

@Service
public class VetSpecialtyMapService extends AbstractMapService<VetSpecialty> implements VetSpecialtyService {
}
